module "vpc" {
  source = "./modules/vpc"
  vpc_cidr = var.vpc_cidr
  name_prefix = var.name_prefix
  public_subnets = var.public_subnets
  availability_zones = var.availability_zones
}

module "sg" {
  source = "./modules/sg"
  vpc_id = module.vpc.vpc_id
  name_prefix = var.name_prefix
}

module "ecr" {
  source = "./modules/ecr"
  security_group_id    = module.sg.app_SG_id
  public_subnets_ids = module.vpc.public_subnets_ids
}


